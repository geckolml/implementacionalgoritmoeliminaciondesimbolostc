S ::= Aa | B
B ::= bB | b | λ
A ::= Aa | bA | BEE
E ::= λ

S -> Aa | B
B -> bB | b | λ
A -> Aa | bA | BEE
E -> λ
